@partitioned
Feature: Third Set Of Test Scenarios

  Scenario Outline: Third Outline Test
    When maven runs
    Then the tests pass
    Examples:
      | first | second |
      | a     | b      |
      | c     | d      |
      | e     | f      |
      | g     | h      |

  Scenario: The First Scenario in the Third Feature File
    When maven runs
    Then the tests pass

  Scenario: The Second Scenario in the Third Feature File
    When maven runs
    Then the tests pass

  Scenario: The Third Scenario in the Third Feature File
    When maven runs
    Then the tests pass

  Scenario: The Fourth Scenario in the Third Feature File
    When maven runs
    Then the tests pass

  Scenario: The Fifth Scenario in the Third Feature File
    When maven runs
    Then the tests pass

  Scenario: The Sixth Scenario in the Third Feature File
    When maven runs
    Then the tests pass

  Scenario: The Seventh Scenario in the Third Feature File
    When maven runs
    Then the tests pass

  Scenario: The Eight Scenario in the Third Feature File
    When maven runs
    Then the tests pass

  Scenario: The Ninth Scenario in the Third Feature File
    When maven runs
    Then the tests pass

  Scenario: The Tenth Scenario in the Third Feature File
    When maven runs
    Then the tests pass