package io.cucumber.junit;


import io.cucumber.core.eventbus.EventBus;
import io.cucumber.core.feature.FeatureParser;
import io.cucumber.core.filter.Filters;
import io.cucumber.core.gherkin.Feature;
import io.cucumber.core.options.CucumberOptionsAnnotationParser;
import io.cucumber.core.options.CucumberProperties;
import io.cucumber.core.options.CucumberPropertiesParser;
import io.cucumber.core.plugin.PluginFactory;
import io.cucumber.core.plugin.Plugins;
import io.cucumber.core.resource.ClassLoaders;
import io.cucumber.core.runtime.*;
import io.cucumber.messages.types.TestRunFinished;
import io.cucumber.messages.types.TestRunStarted;
import io.cucumber.messages.types.Timestamp;
import io.cucumber.plugin.event.TestSourceRead;
import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.ParentRunner;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.RunnerScheduler;

import java.io.IOException;
import java.time.Clock;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

import io.cucumber.core.options.RuntimeOptions;
import org.junit.runners.model.Statement;

/**
 * Created by markshead on 2/28/16.
 */
public class CucumberPartitioned extends ParentRunner<FeatureRunnerPartitioned> {
    private final List<FeatureRunnerPartitioned> children = new ArrayList<>();
    private final EventBus bus;

    private final List<Feature> features;
    private final Plugins plugins;
    private boolean multiThreadingAssumed = false;

    /**
     * Constructor called by JUnit.
     *
     * @param clazz the class with the @RunWith annotation.
     * @throws java.io.IOException                         if there is a problem
     * @throws org.junit.runners.model.InitializationError if there is another problem
     */
    public CucumberPartitioned(Class clazz) throws InitializationError, IOException {
        super(clazz);
        Assertions.assertNoCucumberAnnotatedMethods(clazz);
        RuntimeOptions runtimeOptions = getRuntimeOptions(clazz);
        JUnitOptions junitOptions = getJUnitOptions(clazz);
        this.bus = new TimeServiceEventBus(Clock.systemUTC(), UUID::randomUUID);
        FeatureParser parser = new FeatureParser(this.bus::generateId);
        Supplier<ClassLoader> classLoader = ClassLoaders::getDefaultClassLoader;
        FeaturePathFeatureSupplier featureSupplier =
                new FeaturePathFeatureSupplier(classLoader, runtimeOptions, parser);
        this.features = featureSupplier.get();

        // Create plugins after feature parsing to avoid the creation of empty files on
        // lexer errors.
        this.plugins = new Plugins(new PluginFactory(), runtimeOptions);
        ThreadLocalRunnerSupplier runnerSupplier = CreateRunnerSupplier(classLoader, runtimeOptions, clazz);
        Filters filters = new Filters(runtimeOptions);
        for (Feature feature : features) {
            CucumberExecutionContext context = new CucumberExecutionContext(
                    this.bus,
                    new ExitStatus(runtimeOptions),
                    runnerSupplier);
            FeatureRunnerPartitioned featureRunner = new FeatureRunnerPartitioned(
                    clazz, context, feature, filters, junitOptions);
            if (!featureRunner.isEmpty()) {
                children.add(featureRunner);
            }
        }
    }

    private ThreadLocalRunnerSupplier CreateRunnerSupplier(
            Supplier<ClassLoader> classLoader,
            RuntimeOptions runtimeOptions,
            Class<?> clazz) {
        ObjectFactoryServiceLoader objectFactoryServiceLoader =
                new ObjectFactoryServiceLoader(classLoader, runtimeOptions);
        ObjectFactorySupplier objectFactorySupplier =
                new ThreadLocalObjectFactorySupplier(objectFactoryServiceLoader);
        BackendSupplier backendSupplier =
                new BackendServiceLoader(clazz::getClassLoader, objectFactorySupplier);

        return new ThreadLocalRunnerSupplier(
                        runtimeOptions,
                        bus,
                        backendSupplier,
                        objectFactorySupplier);


    }

    private JUnitOptions getJUnitOptions(Class<?> clazz) {
        // Next parse the junit options
        JUnitOptions junitPropertiesFileOptions =
                new JUnitOptionsParser().parse(CucumberProperties.fromPropertiesFile()).build();

        JUnitOptions junitAnnotationOptions =
                new JUnitOptionsParser().parse(clazz).build(junitPropertiesFileOptions);

        JUnitOptions junitEnvironmentOptions =
                new JUnitOptionsParser()
                        .parse(CucumberProperties.fromEnvironment())
                        .build(junitAnnotationOptions);

        return new JUnitOptionsParser()
            .parse(CucumberProperties.fromSystemProperties())
            .build(junitEnvironmentOptions);
    }

    private RuntimeOptions getRuntimeOptions(Class<?> clazz) {
        RuntimeOptions annotationOptions = new CucumberOptionsAnnotationParser()
                .withOptionsProvider(new JUnitCucumberOptionsProvider())
                .parse(clazz)
                .build();

        RuntimeOptions environmentOptions = new CucumberPropertiesParser()
                .parse(CucumberProperties.fromEnvironment())
                .addDefaultSummaryPrinterIfNotDisabled()
                .build(annotationOptions);

        return new CucumberPropertiesParser()
                .parse(CucumberProperties.fromSystemProperties())
                .addDefaultSummaryPrinterIfNotDisabled()
                .build(environmentOptions);
    }

    @Override
    public List<FeatureRunnerPartitioned> getChildren() {
        return children;
    }

    protected Description describeChild(FeatureRunnerPartitioned child) {
        return child.getDescription();
    }

    protected void runChild(FeatureRunnerPartitioned child, RunNotifier notifier) {
        child.run(notifier);
    }

    @Override
    protected Statement childrenInvoker(RunNotifier notifier) {
        Statement runFeatures = super.childrenInvoker(notifier);
        return new RunCucumber(runFeatures);
    }

    class RunCucumber extends Statement {
        private final Statement runFeatures;

        RunCucumber(Statement runFeatures) {
            this.runFeatures = runFeatures;
        }

        @Override
        public void evaluate() throws Throwable {
            if (multiThreadingAssumed) {
                plugins.setSerialEventBusOnEventListenerPlugins(bus);
            } else {
                plugins.setEventBusOnEventListenerPlugins(bus);
            }

            bus.send(new TestRunStarted(getTimestamp()));
            for (Feature feature : features) {
                bus.send(new TestSourceRead(bus.getInstant(), feature.getUri(), feature.getSource()));
            }
            runFeatures.evaluate();
            bus.send(new TestRunFinished(null, true, getTimestamp()));
        }

        private Timestamp getTimestamp() {
            return new Timestamp(bus.getInstant().toEpochMilli(), bus.getInstant().getEpochSecond());
        }
    }



    @Override
    public void setScheduler(RunnerScheduler scheduler) {
        super.setScheduler(scheduler);
        multiThreadingAssumed = true;
    }

}
