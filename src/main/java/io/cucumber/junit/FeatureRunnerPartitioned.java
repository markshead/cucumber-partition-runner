package io.cucumber.junit;

import io.cucumber.core.filter.Filters;
import io.cucumber.core.gherkin.Feature;
import io.cucumber.core.gherkin.Pickle;
import io.cucumber.core.runtime.CucumberExecutionContext;
import io.cucumber.junit.PickleRunners.PickleRunner;
import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.ParentRunner;
import org.junit.runners.model.InitializationError;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static io.cucumber.junit.PickleRunners.withNoStepDescriptions;
import static io.cucumber.junit.PickleRunners.withStepDescriptions;

final class FeatureRunnerPartitioned extends ParentRunner<PickleRunner> {
    private final List<PickleRunner> children = new ArrayList<>();

    private final Feature feature;
    private Description description;
    private int cucumberPartitionQty;
    private int cucumberPartitionId;
    private boolean debug;

    FeatureRunnerPartitioned(
            Class clazz,
            CucumberExecutionContext context,
            Feature feature,
            Filters filters,
            JUnitOptions jUnitOptions
    ) throws InitializationError {
        super(clazz);
        setPartitionInfo();
        this.feature = feature;
        buildFeatureElementRunners(filters, context, jUnitOptions);
    }

    private void setPartitionInfo() {
        try {
            cucumberPartitionQty = Integer.parseInt(System.getProperty("cucumber.partition.qty"));
            cucumberPartitionId = Integer.parseInt(System.getProperty("cucumber.partition.id"));
            debug = Boolean.parseBoolean(System.getProperty("cucumber.partition.debug","false"));
        } catch (RuntimeException e) {
            printPartitionInstructions();
            e.printStackTrace();
            throw new RuntimeException("CucumberPartition Not Properly Configured");
        }

        if(cucumberPartitionId < 1 ||
                cucumberPartitionQty < 1 ||
                cucumberPartitionId > cucumberPartitionQty) {
            printPartitionInstructions();
            throw new RuntimeException("CucumberPartition Not Properly Configured");
        }
    }

    private void printPartitionInstructions() {
        System.out.println("To use CucumberPartitioned, you must pass two system variables. For example:");
        System.out.println("-Dcucumber.partition.id=2 -Dcucumber.partition.qty=5");
        System.out.println("cucumber.partition.qty is the total number of partitions");
        System.out.println("cucumber.partition.id is the id of the particular partition");
        System.out.println("There should be an environment with in id from 1 through the qty of partitions.");
        System.out.println("cucumber.partition.id and cucumber.partition.qty must be present and greater than 0");
        System.out.println("cucumber.partition.id cannot be more than cucumber.partition.qty");
    }
    @Override
    protected String getName() {
        return feature.getKeyword() + ": " + feature.getName();
    }

    @Override
    public Description getDescription() {
        if (description == null) {
            description = Description.createSuiteDescription(getName(), new FeatureId(feature));
            for (PickleRunner child : getChildren()) {
                description.addChild(describeChild(child));
            }
        }
        return description;
    }

    public boolean isEmpty() {
        return children.isEmpty();
    }

    @Override
    protected List<PickleRunner> getChildren() {
        return children;
    }

    @Override
    protected Description describeChild(PickleRunner child) {
        return child.getDescription();
    }

    @Override
    protected void runChild(PickleRunner child, RunNotifier notifier) {
        child.run(notifier);
    }

    private void buildFeatureElementRunners(
            Filters filters,
            CucumberExecutionContext context,
            JUnitOptions jUnitOptions) {
        for (int i = 0; i < feature.getPickles().size(); i++) {
            Pickle pickle = feature.getPickles().get(i);
            if (filters.test(pickle)) {
                int assignToPartition = Math.abs(pickle.getName().hashCode()) % cucumberPartitionQty + 1;
                if(debug) {
                    System.out.print(cucumberPartitionId == assignToPartition ? "*" : " ");
                    System.out.print("Partition " + assignToPartition);
                    System.out.println(" assigned to " + pickle.getName());
                }

                if(cucumberPartitionId == assignToPartition) {
                    if (jUnitOptions.stepNotifications()) {
                        PickleRunner picklePickleRunner;
                        picklePickleRunner = withStepDescriptions(context, pickle, i, jUnitOptions);
                        children.add(picklePickleRunner);
                    } else {
                        PickleRunner picklePickleRunner;
                        picklePickleRunner = withNoStepDescriptions(
                                feature.getName().orElse(String.format("Pickle %d", i)),
                                context,
                                pickle,
                                i,
                                jUnitOptions);
                        children.add(picklePickleRunner);
                    }
                }
            }
        }
    }

    private static final class FeatureId implements Serializable {
        private static final long serialVersionUID = 1L;
        private final URI uri;

        FeatureId(Feature feature) {
            this.uri = feature.getUri();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            FeatureId featureId = (FeatureId) o;
            return uri.equals(featureId.uri);

        }

        @Override
        public int hashCode() {
            return uri.hashCode();
        }

        @Override
        public String toString() {
            return uri.toString();
        }
    }

}
